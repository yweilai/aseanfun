import React, { Component } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MainScreen from '../screens/Main';
import SpotCategoriesScreen from '../screens/SpotCategories';
import SpotByCategoryScreen from '../screens/SpotByCategory';
import PostsByCategoryScreen from '../screens/PostsByCategory';
import PlaceDetailsScreen from '../screens/SpotDetails';
import PostDetailsScreen from '../screens/PostDetails';
import CategoryByTypeScreen from '../screens/CategoryByType';
import PersonalScreen from "../screens/Personal";
import MessageScreen from "../screens/Message";
import LogoutScreen from "../screens/Logout";
import SettingsScreen from "../screens/Settings";
import TermsScreen from "../screens/Terms";
import AboutUsScreen from "../screens/AboutUs";
import ContactUsScreen from "../screens/ContactUs";
import SearchScreen from "../screens/Search";
import CategoriesScreen from "../screens/Categories";
import { Ionicons } from '@expo/vector-icons';
import Strings from '../utils/Strings';

var styles = require('../../assets/files/Styles');

const Stack = createStackNavigator();

export default function Logged(props){

  const {navigation} = props;

  const navigationOptions = {
    headerStyle: styles.headerStyle,
    headerBackTitle: null,
    headerTintColor: '#fff',
    headerTitleAlign: 'center',
    headerTitleStyle: {
      textAlign: 'center',
      alignSelf: 'center',
      justifyContent: 'space-between',
      fontSize: 16,
      color: '#fff',
      fontWeight: 'bold'
    },
    headerBackTitleVisible:false,
  }

const buttonLeft = () => {
  return (
    <Ionicons name={'md-arrow-back'} onPress={ () => { navigation.goBack() }} style={styles.arrowbackicon}/>
    )
};

return (
	<Stack.Navigator screenOptions={navigationOptions}>
	<Stack.Screen name="MainScreen" component={MainScreen} options={{ headerShown: false }}/>
  <Stack.Screen name="SpotCategoriesScreen" component={SpotCategoriesScreen} options={{ title: Strings.ST1 }}/>
  <Stack.Screen name="SpotByCategoryScreen" component={SpotByCategoryScreen}/>
  <Stack.Screen name="PostsByCategoryScreen" component={PostsByCategoryScreen}/>
  <Stack.Screen name="PlaceDetailsScreen" component={PlaceDetailsScreen}/>
  <Stack.Screen name="PostDetailsScreen" component={PostDetailsScreen}/>
  <Stack.Screen name="CategoryByTypeScreen" component={CategoryByTypeScreen}/>
  <Stack.Screen name="PersonalScreen" component={PersonalScreen} options={{ title: Strings.ST6 }}/>
  <Stack.Screen name="MessageScreen" component={MessageScreen} options={{ title: Strings.ST46 }}/>
  <Stack.Screen name="LogoutScreen" component={LogoutScreen}/>
  <Stack.Screen name="SettingsScreen" component={SettingsScreen} options={{ title: Strings.ST7 }}/>
  <Stack.Screen name="TermsScreen" component={TermsScreen} options={{ title: Strings.ST82 }}/>
  <Stack.Screen name="AboutUsScreen" component={AboutUsScreen} options={{ title: Strings.ST9 }}/>
  <Stack.Screen name="ContactUsScreen" component={ContactUsScreen} options={{ title: Strings.ST73 }}/>
  <Stack.Screen name="SearchScreen" component={SearchScreen} options={{ title: Strings.ST19 }}/>
  <Stack.Screen name="CategoriesScreen" component={CategoriesScreen} options={{ title: Strings.ST98 }}/>
	</Stack.Navigator>
	)

}
